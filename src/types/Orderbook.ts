type Signature = {
    r: string
    s: string
    signatureType: number
    v: number
}

type Order = {
    chainId: number
    expiry: string
    feeRecipient: string
    maker: string
    makerAmount: string
    makerToken: string
    pool: string
    salt: string
    sender: string
    signature: Signature
    taker: string
    takerAmount: string
    takerToken: string
    takerTokenFeeAmount: string
    verifyingContract: string
}

type MetaData = {
    createdAt: string
    orderHash: string
    remainingFillableTakerAmount: string
}

type TRecord = {
    metaData: MetaData
    order: Order
}

type Orders = {
    page: number
    perPage: number
    records: TRecord[]
    total: number
}

type OrderBook = {
    asks: Orders
    bids: Orders
}

export type { OrderBook, Orders, TRecord }
