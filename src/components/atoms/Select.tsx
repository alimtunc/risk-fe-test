'use client'

import { Dispatch, SetStateAction } from 'react'
import RSelect, { SingleValue } from 'react-select'

type Option = {
    label: string
    value: string
}

interface SelectProps {
    onChange: Dispatch<SetStateAction<string | undefined>>
    options: Option[]
    title: string
}

const Select = (props: SelectProps): JSX.Element => {
    const { options, title, onChange } = props

    return (
        <div className="z-20 w-full">
            <div className="font-2 mb-2">{title}</div>
            <RSelect
                options={options}
                onChange={(newValue: SingleValue<Option>) => onChange(newValue?.value)}
                theme={(theme) => ({
                    ...theme,
                    colors: {
                        ...theme.colors,
                        primary25: '#b5acac',
                        primary: '#c2cf36',
                    },
                })}
            />
        </div>
    )
}

export { Select }
