'use client'

import { useMemo } from 'react'
import React from 'react'

import { OrderBook, TRecord } from '@/types/Orderbook'

type Detail = {
    price: number
    quantity: number
    total: number
}

interface ordersProps {
    orderbook?: OrderBook
}

const getValues = (records: TRecord[]) => {
    let total = 0

    const details: Detail[] = []

    records.forEach((record) => {
        const { makerAmount, takerAmount } = record.order
        const makerAmountNumber = Number(makerAmount)
        const takerAmountNumber = Number(takerAmount)

        total += Number(makerAmount)

        details.push({
            price: takerAmountNumber / makerAmountNumber,
            quantity: makerAmountNumber,
            total,
        })
    })

    return { details, total }
}

const OrdersList = (props: ordersProps): JSX.Element => {
    const { orderbook } = props

    const ordersData = useMemo(() => {
        if (!orderbook) return

        const { details: askDetails, total: totalAsks } = getValues(orderbook.asks.records)
        const { details: bidDetails, total: totalBids } = getValues(orderbook.bids.records)

        return {
            totalAsks,
            askDetails,
            totalBids,
            bidDetails,
        }
    }, [orderbook])

    return ordersData && ordersData.totalAsks > 0 && ordersData.totalBids > 0 ? (
        <div className="z-10 mt-10 flex w-full max-w-6xl flex-col items-center justify-center gap-20 rounded-lg bg-secondary bg-opacity-80 p-6 backdrop-blur lg:flex-row">
            <div>
                <h3 className="font-2 mb-4 text-center">Asks</h3>
                <div className="grid grid-cols-3 gap-4">
                    <p className="font-2 text-right">Price</p>
                    <p className="font-2 text-right">Quantity</p>
                    <p className="font-2 text-right">Total</p>
                    {ordersData.askDetails.map((detail: Detail, index: number) => {
                        return (
                            <React.Fragment key={`ask-detail-${index}`}>
                                <p className="font-1 text-right">{detail.price.toFixed(10).toLocaleString()}</p>
                                <p className="font-1 text-right">{detail.quantity.toLocaleString()}</p>
                                <div className="relative">
                                    <p className="font-1 relative z-[1] text-right">{detail.total.toLocaleString()}</p>
                                    <div
                                        style={{
                                            width: `${
                                                (parseFloat(detail.total.toFixed(10)) /
                                                    parseFloat(ordersData.totalAsks.toFixed(10))) *
                                                100
                                            }%`,
                                        }}
                                        className={`absolute right-0 top-0 h-full  bg-green-500`}
                                    ></div>
                                </div>
                            </React.Fragment>
                        )
                    })}
                </div>
            </div>
            <div>
                <h3 className="font-2 mb-4 text-center">Bids</h3>
                <div className="grid grid-cols-3 gap-4">
                    <p className="font-2">Total</p>
                    <p className="font-2">Quantity</p>
                    <p className="font-2">Price</p>
                    {ordersData.bidDetails.map((detail: Detail, index: number) => {
                        return (
                            <React.Fragment key={`bids-detail-${index}`}>
                                <div className="relative w-fit">
                                    <p className="font-1 relative z-[1]">{detail.total.toLocaleString()}</p>
                                    <div
                                        style={{
                                            width: `${
                                                (parseFloat(detail.total.toFixed(10)) /
                                                    parseFloat(ordersData.totalBids.toFixed(10))) *
                                                100
                                            }%`,
                                        }}
                                        className={`absolute left-0 top-0 h-full bg-red-500`}
                                    ></div>
                                </div>
                                <p className="font-1">{detail.quantity.toLocaleString()}</p>
                                <p className="font-1">{detail.price.toFixed(10).toLocaleString()}</p>
                            </React.Fragment>
                        )
                    })}
                </div>
            </div>
        </div>
    ) : (
        <p className="font-3 mt-6">Select pairs to show data 💰</p>
    )
}

export { OrdersList }
