'use client'

import Image from 'next/image'
import { useState } from 'react'
import { useQuery } from 'react-query'

import { OrdersList } from '@/components/atoms/Orders'
import { Select } from '@/components/atoms/Select'

import TokenPairs from '@/data/tokenPairs.json'

import { OrderBook } from '@/types/Orderbook'

export default function Home() {
    const [quoteToken, setQuoteToken] = useState<string | undefined>()
    const [baseToken, setBaseToken] = useState<string | undefined>()

    const { isLoading, error, data } = useQuery<OrderBook>({
        queryKey: ['ordersData', quoteToken, baseToken],
        queryFn: () =>
            fetch(`https://api.0x.org/orderbook/v1?quoteToken=${quoteToken}&baseToken=${baseToken}`).then((res) =>
                res.json()
            ),
        enabled: quoteToken !== undefined && baseToken !== undefined,
    })

    if (error) return 'An error has occurred'

    return (
        <div className="relative flex h-full w-full items-center justify-center bg-secondary">
            <div className="solid flex h-[60%] flex-col items-center justify-center rounded-lg border border-solid border-secondary px-8  py-6 md:w-10/12">
                <div className="flex w-full gap-4 sm:w-8/12 lg:w-4/12">
                    <Select title="Pay with" options={TokenPairs} onChange={setQuoteToken} />
                    <Select
                        title="Receive"
                        options={TokenPairs.filter((token) => token.value !== quoteToken)}
                        onChange={setBaseToken}
                    />
                </div>
                {isLoading ? (
                    <Image priority src={'/images/loader.svg'} height={300} width={300} alt="Loading orders..." />
                ) : (
                    <OrdersList orderbook={data} />
                )}
            </div>
        </div>
    )
}
