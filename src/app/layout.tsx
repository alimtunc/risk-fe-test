'use client'

import { Montserrat } from 'next/font/google'
import { useCallback } from 'react'
import { QueryClient, QueryClientProvider } from 'react-query'
import Particles from 'react-tsparticles'
import { loadFull } from 'tsparticles'
import type { Engine } from 'tsparticles-engine'

import particles from '@/data/particles.json'

import './globals.css'

const montserrat = Montserrat({
    subsets: ['latin'],
    variable: '--font-roboto',
})

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            refetchOnWindowFocus: false,
        },
    },
})

export default function RootLayout({ children }: { children: React.ReactNode }) {
    const particlesInit = useCallback(async (engine: Engine) => await loadFull(engine), [])

    return (
        <html lang="en" className={montserrat.className}>
            <body>
                <QueryClientProvider client={queryClient}>
                    {children}
                    <Particles id="tsparticles" options={particles} init={particlesInit} />
                </QueryClientProvider>
            </body>
        </html>
    )
}
