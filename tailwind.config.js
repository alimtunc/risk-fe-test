/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ['./src/app/**/*.{js,ts,jsx,tsx,mdx}', './src/components/**/*.{js,ts,jsx,tsx,mdx}'],
    theme: {
        extend: {
            fontFamily: {
                montserrat: ['var(--font-montserrat)'],
            },
            colors: {
                primary: '#daed06',
                secondary: '#1a1a1a',
            },
        },
    },
    plugins: [],
}
